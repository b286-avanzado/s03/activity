from django.shortcuts import render
from django.http import HttpResponse
from .models import GroceryItem
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def index(request):
    groceryitem_list = GroceryItem.objects.all()
    context = {'groceryitem_list' : groceryitem_list}
    return render(request, "django_practice/index.html", context)

def groceryitem(request, groceryitem_id):
    response = "You are viewing the details of %s"
    return HttpResponse(response %groceryitem_id)


def register(request):
    users = User.objects.all()
    is_user_registered = False
    context = {
        "is_user_registered": is_user_registered
    }

    for indiv_user in users:
        if indiv_user.username == "user":
            is_user_registered = True
            break

    if is_user_registered == False:
        user = User()
        user.username = "user"
        user.first_name = "User"
        user.last_name = "User"
        user.email = "user@mail.com"
        user.set_password("user123")
        user.is_staff = False
        user.is_active = True
        user.save()

        context = {
            "first_name": user.first_name,
            "last_name": user.last_name
        }

    return render(request, "django_practice/register.html", context)
        
def change_password(request):
    is_user_authenticated = False

    user = authenticate(username="user", password="user123")
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username='user')
        authenticated_user.set_password("useruser")
        authenticated_user.save()
        is_user_authenticated = True
    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "django_practice/change_password.html", context)

def login_view(request):
    username = "user"
    password = "useruser"
    user = authenticate(username = username, password = password)
    context = {
        "is_user_authenticated" : False
    }
    print(user)
    if user is not None:
        login(request, user)
        return redirect("index")
    else:
        return render(request, "django_practice/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("index")
